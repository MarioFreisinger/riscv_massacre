module top (
	input        clk,
	input        key,
  input        rst,
	output [6:0] led
);
   
   reg    ctr_q;
   reg [25:0] counter_ff;
 

   // Sequential code (flip-flop)
  always @(posedge clk) begin
	    if (~rst) begin
         ctr_q <= 1'b0;
         counter_ff <= 'b0;
      end else begin
         if (counter_ff[25] == 1'b1) begin 
            ctr_q <= ~ctr_q;
            counter_ff <= 'b0;
         end else begin
           ctr_q <= ctr_q;
         end
            
         counter_ff <= counter_ff + 1;
     
      end
  end
   
   assign led[1] = ~ctr_q;
   
   
endmodule
