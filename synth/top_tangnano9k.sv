module top_tangnano9k (
                       input logic        clk,
                       input logic        rst_n,
                       output logic [5:0] led,
                       input logic        uart_rx,
                       output logic       uart_tx,
                       input logic        s2_i,
                       output logic       pin_49_o,
                       output logic       pin_48_o
                       //input logic uart_we,
                       //input logic [31:0] uart_tr_dat,
                       //output logic uart_dat_wait
                       );
   
   logic [31:0]                           mem_data;
   massacre_main m_massacre_main (.clk_i (clk),      
                                  .reset_i (~rst_n),
                                  .mem_data_o (mem_data)
                                  );

   //--------------------------------------------------------------
   //UART
   logic fpga_tx;
   // assign led and output pin 49 with uart tx for debugging
   assign led[0] = fpga_tx;
   assign pin_49_o = fpga_tx;
   assign uart_tx = fpga_tx;
   
   own_uart #(.DIV(234)) m_own_uart
     (
      .clk(clk),
      .rst_n(rst_n),
      .send_trigger(s2_i),
      .send_data(8'd66),
      .tx(fpga_tx),
      .rx(uart_rx),
      .uart_clk(pin_48_o)
      );
   //--------------------------------------------------------------
   
endmodule
