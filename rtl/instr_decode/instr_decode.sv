import massacre_pkg::*;

module instr_decode (
                     input clk_i,
                     input reset_i,

                     input [31:0] instr_i,

                     output add_o,
                     output opcode_type_compressed curr_type_o
                     );
   
   logic [31:0]    instr_ff;

   logic [4:0]     rs1, rs2, rd;
   logic [2:0]     func3;
   logic [6:0]     func7;
   logic [31:0]    imm;

   opcode_type_compressed curr_type; 

   always_ff @(posedge clk_i, reset_i) begin
      if (reset_i) begin
         instr_ff <= '0;
      end else begin
         instr_ff <= change_endianess(instr_i);
      end
   end

   // Type of instruction decode sorce registers and immeadiate accordingly
   always_comb begin
      imm = '0;
      rs1 = '0;
      rs2 = '0;
      rd = '0;
      func3 = '0;
      func7 = '0;
      curr_type = R_TYPE_C;
      
      case (instr_ff[6:0])
        
        R_TYPE: begin
           rs1 = instr_ff[19:15];
           rs2 = instr_ff[24:20];
           rd = instr_ff[11:7];
           func3 = instr_ff[14:12];
           func7 = instr_ff[31:25];
           curr_type = R_TYPE_C;
        end

        
        I_TYPE: begin
           imm = 32'(signed'(instr_ff[31:20]));
           rs1 = instr_ff[19:15];
           rs2 = instr_ff[24:20];
           rd = instr_ff[11:7];
           func3 = instr_ff[14:12];
           curr_type = I_TYPE_C;
        end

        S_TYPE: begin
           imm = 32'(signed'({instr_ff[31:25], instr_ff[11:7]}));
           rs1 = instr_ff[19:15];
           rs2 = instr_ff[24:20];
           func3 = instr_ff[14:12];
           curr_type = S_TYPE_C;
        end


        B_TYPE: begin
           imm = 32'(signed'({instr_ff[31], instr_ff[7],
                              instr_ff[30:25], instr_ff[11:8]}));
           rs1 = instr_ff[19:15];
           rs2 = instr_ff[24:20];
           func3 = instr_ff[14:12];
           curr_type = B_TYPE_C;
        end
                  

        U_TYPE: begin
           imm = {instr_ff[31:12], 12'b0};
           rd = instr_ff[11:7];
           curr_type = U_TYPE_C;
        end


        J_TYPE: begin
           imm = 32'(signed'({instr_ff[31], instr_ff[19:12], instr_ff[20],
                              instr_ff[30:21], 1'b0}));
           rd = instr_ff[11:7];
           curr_type = J_TYPE_C;
        end
           
      endcase
   end
     
   function [31:0] change_endianess;
      input [31:0] val;
      begin
         change_endianess = {val[7:0], val[15:8], val[23:16], val[31:24]};
      end
   endfunction // change_endianl
   

endmodule
