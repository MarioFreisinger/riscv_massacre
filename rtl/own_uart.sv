module own_uart #(parameter integer DIV = 234)
   (
    input logic       clk,
    input logic       rst_n,
    input logic       send_trigger,
    input logic [7:0] send_data,
    output logic      tx,
    input logic       rx,
    output logic      uart_clk // just for debuggin a putput for each bit of data transferred by the uart
    );

   logic [11:0] divcnt;
   logic [9:0]  send_pattern;
   logic [$clog2(8):0] curr_byte; // count to 9 for one trasmition of
                                 // 8 bit data with start and stop bit
   
   // uart transmit for debugging
   always_ff @(posedge clk, rst_n) begin
      if (~rst_n) begin
         divcnt <= 'b0;
         tx <= 1'b1;
         send_pattern <= {1'b1, 8'd82, 1'b0};
         curr_byte <= 'd0;
         uart_clk <= 'b0;
      end else if (~send_trigger) begin
         divcnt <= 'b0;
         tx <= 1'b1;
         send_pattern <= {1'b1, send_data, 1'b0};
         curr_byte <= 'd0;
      end else if (curr_byte <= 'd9) begin
        divcnt <= divcnt + 1;
        if (divcnt == 12'd234) begin
           divcnt <= 0;
           tx <= send_pattern[0];
           send_pattern <= send_pattern >> 1;
           curr_byte += 1;
           uart_clk <= ~uart_clk;
        end
      end else begin
         tx <= 'b1;
      end // else: !if(curr_byte <= 'd9)
   end // always_ff @ (posedge clk, rst_n)

endmodule // own_uart
