module massacre_main #( parameter bit_width = 32 
                        )

                      (
                       input logic         clk_i,
                       input logic         reset_i,
                       output logic [31:0] mem_data_o
                      );
   


   logic [bit_width-1:0]     mem_addr_fetch_con;
   logic [bit_width-1:0]     mem_data_fetch_con;

   assign mem_data_o = mem_data_fetch_con;
   
   inst_fetch m_inst_fetch ( .clk_i (clk),
                             .reset_i (rst),
                
                             .mem_addr_o (mem_addr_fetch_con)
                
                             );


   instr_decode m_instr_decode (.clk_i (clk),
                               .reset_i (rst),
                               .instr_i (mem_data_fetch_con),
                               .add_o ()
                               );
   

                
   mem m_mem ( .clk_i (clk),
               .reset_i (rst),
               .addr_i (mem_addr_fetch_con),
               .data_i (),
               .write_i ('0),
               .data_o (mem_data_fetch_con)

               );
   
endmodule
