package massacre_pkg;
   


   typedef enum logic [6:0] {
                             
                             R_TYPE = 7'b0110011,
                             I_TYPE = 7'b0010011,
                             S_TYPE = 7'b0100011,
                             B_TYPE = 7'b1100011,
                             U_TYPE = 7'b0110111,
                             J_TYPE = 7'b1101111
                             
                             } opcode_type;


   typedef enum logic [2:0] {
                             
                             R_TYPE_C,
                             I_TYPE_C,
                             S_TYPE_C,
                             B_TYPE_C,
                             U_TYPE_C,
                             J_TYPE_C
                             
                             } opcode_type_compressed;


   typedef enum logic [2:0] {
                             ADDI,
                             SLLI,
                             SLTI,
                             SLTIU,
                             XORI,
                             SRLI,
                             ORI,
                             ANDI
                             } i_type_func3_codes;
                         
                           
endpackage
