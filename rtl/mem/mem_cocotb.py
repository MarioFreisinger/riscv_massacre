# This file is public domain, it can be freely copied without restrictions.
# SPDX-License-Identifier: CC0-1.0

import os
import random
from pathlib import Path

import cocotb
from cocotb.clock import Clock
from cocotb.runner import get_runner
from cocotb.triggers import RisingEdge
from cocotb.types import LogicArray

def test_simple_dff_runner():
    hdl_toplevel_lang = os.getenv("HDL_TOPLEVEL_LANG")
    sim = os.getenv("SIM")
    print(sim)
    
    #os.environ["EXTRA_ARGS"] = "--trace-fst --trace-structs"
    #os.environ["WAVES"] = "1"
    args = os.getenv("EXTRA_ARGS")
    print(args)
    args = os.getenv("WAVES")
    print(args)

    proj_path = Path(__file__).resolve().parent

    verilog_sources = []
    vhdl_sources = []

    verilog_sources = [proj_path / "mem.sv"]

    runner = get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
        vhdl_sources=vhdl_sources,
        hdl_toplevel="mem",
        always=True,
    )

    runner.test(hdl_toplevel="mem", test_module="test_mem")
   


if __name__ == "__main__":
    test_simple_dff_runner()
 
