
module mem #(
             parameter byte_size = 2048
             )
  
   (
    input logic         clk_i, 
    input logic         reset_i,
    input logic [31:0]  addr_i,
    input logic [31:0]  data_i,
    input logic         write_i,
    output logic [31:0] data_o
    );

   
   parameter physical_bit_width = $clog2(byte_size)-1;
   logic [31:0] mem [2**(physical_bit_width)];   

   initial begin
      $readmemh("/home/mario/uni/master/own_projects/riscv_massacre/C/verilog_cor.hex",
                mem);
   end

   //$DISPLAY("jd");
   
   logic [physical_bit_width-1:0] physical_addr;
   assign physical_addr = addr_i[physical_bit_width+1:2];
     
   always_ff @(posedge clk_i) begin
      if (write_i == 'b1) begin
            mem[physical_addr] <= data_i;
      end

   end //always_ff
   
   always_comb begin
      data_o = mem[physical_addr];
   end
   

endmodule // mem
         

            
