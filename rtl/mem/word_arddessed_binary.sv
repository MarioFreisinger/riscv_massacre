module convert_mem
(
);
   parameter byte_mem_width = $clog2(2048);
   
   logic [31:0] byte_mem [integer];
   logic [31:0] word_mem [integer];

   /*always_comb begin
      for (int i = 0; i < 1500; i++) begin
         word_mem[i] = byte_mem[i+384];
      end
   end*/ // UNMATCHED !!

   always_comb begin
      for (int i = 0; i < 200; i++) begin
         word_mem[i] = byte_mem[i+384];
      end
   end
   
   initial begin
      $readmemh("/home/mario/uni/master/own_projects/riscv_massacre/C/verilog.hex",
                byte_mem);
      #20
        $writememh("/home/mario/uni/master/own_projects/riscv_massacre/C/verilog_cor.hex"                 , word_mem);
      
      #20 $finish;
      
      
   end

   

endmodule
   
