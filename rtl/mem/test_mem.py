import os
import random
from pathlib import Path

import cocotb
from cocotb.clock import Clock
from cocotb.runner import get_runner
from cocotb.triggers import RisingEdge
from cocotb.types import LogicArray


@cocotb.test()
async def test_mem(dut):
    dut.reset_i.value = 0
    dut.write_i.value = 0
    
    clock = Clock(dut.clk_i, 10, units="us")  
    cocotb.start_soon(clock.start(start_high=False))

    await RisingEdge(dut.clk_i)
    dut.reset_i.value = 1
    await RisingEdge(dut.clk_i)
    dut.reset_i.value = 0

    # Synchronize with the clock. This will regisiter the initial `d` value
    await RisingEdge(dut.clk_i)

    dut.addr_i.value = 0x10
    dut.write_i.value = 1
    dut.data_i.value = 0x12345678

    await RisingEdge(dut.clk_i)
    dut.addr_i.value = 0x10
    dut.write_i.value = 0
    
    await RisingEdge(dut.clk_i)
    assert dut.data_o == 0x12345678, "wrong memory read after write test"

    await RisingEdge(dut.clk_i)
    # read first instruciton
    for i in range(0x200, 0x400, 0x1):
        dut.addr_i.value = i
        dut.write_i.value = 0
        await RisingEdge(dut.clk_i)
        #print(dut.data_o.value)
    
    await RisingEdge(dut.clk_i)
    while(1):
        await RisingEdge(dut.clk_i)
    
    '''for i in range(10):
        val = random.randint(0, 1)
        dut.d. = val  # Assign the random value val to the input port d
        await RisingEdge(dut.clk)
        assert dut.q.value == expected_val, f"output q was incorrect on the {i}th cycle"
        expected_val = val  # Save random value for next RisingEdge

    # Check the final input on the next clock
    await RisingEdge(dut.clk)
    assert dut.q.value == expected_val, "output q was incorrect on the last cycle"
'''
