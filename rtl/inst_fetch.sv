module inst_fetch #( parameter bit_width = 32 )
                   ( input clk_i,
                     input reset_i,

                     output logic [bit_width-1:0] mem_addr_o
                   
                   );


   logic [31:0] program_counter, next_pc;

   assign mem_addr_o = program_counter;
   

   always_comb begin
      next_pc = program_counter + 32'd4;
   end

   always_ff @(posedge clk_i, reset_i) begin
      if (reset_i == '1) begin
         program_counter <= 'h200;
      end else begin
         program_counter <= next_pc;
      end
      
   end

endmodule
   
