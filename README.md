# RISCV_MASSACRE

## RISCV INSTR

### 1. install yosys-systemverilog

This extension of yosys is required to support systemverilog 2017 language structures.
This extension can be loaded by yosys and read_systemveriolog is available then.

```
sudo pacman -Syu curl jq tar wget

curl https://api.github.com/repos/antmicro/yosys-systemverilog/releases/latest | jq .assets[1] | grep "browser_download_url" | grep -Eo 'https://[^\"]*' | xargs wget -O - | tar -xz

cd yosys_systemverilog
./build_binaries.sh
./install_plugins.sh
```

Add the image/bin folder which containes the compiled yosys version and the systemverilog plugin
to th PATH and use the yosys version which comes with the systemverilog plugin and is in the image folder after building the source files.


### 2. The makefile inside the synth folder can be used to synth and create the bitstream file that is flashed on to the tangnano9k
```
cd synth
make
```

The systemverilog extension can be added to yosys by doing the following stuff:
```
yosys
plugin -i systemverilog
read_systemverilog <files to read>
```

Without the makefile one can also play around with the differnet steps until the bitstream file is created inside yosys.

### 3. To flash the bitstream onto the tangnano9k without root prilages the current user has to have access to usb-devices

Usb-access can be granted like this:
```
sudo echo "SUBSYSTEM=="usb", GROUP="usb_access", MODE="0660"" > /etc/udev/rules.d/99-usb-permissions.rules

sudo groupadd usb_access
sudo usermod -aG usb_access <username>
sudo udevadm control --reload-rules
```

The self compiled yosys binaray iside the image filde that was created by the commands above has to be used.

