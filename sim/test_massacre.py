import os
import random
from pathlib import Path

import cocotb
from cocotb.clock import Clock
from cocotb.runner import get_runner
from cocotb.triggers import RisingEdge
from cocotb.types import LogicArray
from cocotbext.uart import UartSource, UartSink


@cocotb.test()
async def test_massacre(dut):
    dut.rst_n.value = 1    
    clock = Clock(dut.clk, 83, units="ns")  
    cocotb.start_soon(clock.start(start_high=False))

    uart_sink = UartSink(dut.uart_tx, baud=38400, bits=8)
    uart_source = UartSource(dut.uart_rx, baud=38400, bits=8)
    
    await RisingEdge(dut.clk)
    dut.rst_n.value = 0
    await RisingEdge(dut.clk)
    dut.rst_n.value = 1

    await RisingEdge(dut.clk)

    #await uart_source.write(b't')
    # wait for operation to complete (optional)
    #await uart_source.wait()
    
    await RisingEdge(dut.clk)
    # recive date from uart device
    data = await uart_sink.read()

    print(data)
