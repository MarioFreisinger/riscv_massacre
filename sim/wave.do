onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_tangnano9k/clk
add wave -noupdate -group massacre /top_tangnano9k/m_massacre_main/mem_data_fetch_con
add wave -noupdate -group massacre /top_tangnano9k/m_massacre_main/mem_addr_fetch_con
add wave -noupdate -group Mem /top_tangnano9k/m_massacre_main/m_mem/addr_i
add wave -noupdate -group Mem /top_tangnano9k/m_massacre_main/m_mem/data_i
add wave -noupdate -group Mem /top_tangnano9k/m_massacre_main/m_mem/write_i
add wave -noupdate -group Mem /top_tangnano9k/m_massacre_main/m_mem/data_o
add wave -noupdate -group Mem /top_tangnano9k/m_massacre_main/m_mem/physical_addr
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/add_o
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/instr_ff
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/imm
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/func3
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/func7
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/rd
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/rs2
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/rs1
add wave -noupdate -group inst_decode /top_tangnano9k/m_massacre_main/m_instr_decode/curr_type
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/uart_clk
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/tx
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/rx
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/send_data
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/send_pattern
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/curr_byte
add wave -noupdate -expand -group own_uart /top_tangnano9k/m_own_uart/send_trigger
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {47996050 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 309
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {280282276 ps}
run -a